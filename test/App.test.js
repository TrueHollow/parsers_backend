const chai = require('chai');
chai.use(require('chai-http'));

const { expect } = chai;
const { app, listener, backend } = require('../src/index');

describe('App test', () => {
  before(async () => {});
  after(async () => {
    listener.close();
    await backend.Clean();
  });
  it('test GET method for /', async () => {
    const agent = chai.request.agent(app);
    const res = await agent.get('/');
    expect(res).to.have.status(200);
    expect(res).to.be.json;
    agent.close();
  });
  it('test GET method for /jobs', async () => {
    const agent = chai.request.agent(app);
    const pricesRes = await agent.post('/api/v1/insurance/prices').send({});
    const { body: jobBody } = pricesRes;
    const res = await agent.get('/jobs');
    expect(res).to.have.status(200);
    expect(res).to.be.json;
    /**
     * @type {Array<Object>}
     */
    const { body } = res;
    expect(body).to.be.an('array');
    const jobSch = body.find(b => b.id === jobBody.id);
    expect(jobSch).to.be.an('object');
    agent.close();
  });
  it('test POST method for /api/v1/insurance/prices (JSON)', async () => {
    const agent = chai.request.agent(app);
    const res = await agent.post('/api/v1/insurance/prices').send({});
    expect(res).to.have.status(200);
    expect(res).to.be.json;
    agent.close();
  });
  it('test POST method for /api/v1/insurance/prices (JSON) CalifAAAINsurance test', async () => {
    const agent = chai.request.agent(app);
    const testObject = {
      txtFirstName: 'Anatoly',
      txtLastName: 'Russkih',
      txtAddressStreet: 'Vostochny',
      txtAddressZip: '90001',
      ddlAddressCity: 'Los Angeles',
      ddlPhoneType: 'Home', // optional
      txtPhone: '4951235678',
      txtEmailAddress: 'example_abs@mdomain.com',
      homeownersDiscount: 1, // if set to 1 (Number)
      ddlVehicleYear: '2000',
      ddlVehicleMake: 'BENTLEY',
      ddlVehicleModel: 'CONTINENTAL R',
      txtAnnualMiles: '4500',
      gender: 'male',
      ddlMaritalStatus: 'Married',
      txtDOB: '08101988',
      coverageLimit: 'low', // medium or high
    };
    await agent.post('/api/v1/insurance/prices').send(testObject);
    agent.close();
  });
});
