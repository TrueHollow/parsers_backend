const genericPool = require('generic-pool');
const Worker = require('./Worker');
const Job = require('./Job');
const ExampleParser = require('./Parsers/ExampleParser');
const CalifAAAInsurance = require('./Parsers/CalifAAAInsurance');
const logger = require('../common/Logger')('src/services/Backend.js');
const config = require('../config');

const factory = {
  create() {
    return new Worker();
  },
  /**
   * Destroy worker
   * @param {Worker} worker
   */
  destroy(worker) {
    worker.clean();
  },
};

class Backend {
  constructor() {
    this.name = `Backend: Puppeteer scripts`;
    this.workersPool = genericPool.createPool(
      factory,
      config.Backend.Pool.options
    );
    this.jobs = new Map();

    /**
     * Array of Parsers constructors
     * @type {Array<ParserBase>}
     */
    this.parsers = [ExampleParser, CalifAAAInsurance];
  }

  /**
   * Additional cleanup (for `npm test`)
   * @return {Promise<Object>}
   */
  async Clean() {
    return this.workersPool.drain().then(() => this.workersPool.clear());
  }

  /**
   * Get current running jobs
   * @return {Array<Job>}
   * @constructor
   */
  get Jobs() {
    return [...this.jobs.values()];
  }

  /**
   * Get current connected parsers
   * @return {Array<ExampleParser>}
   */
  get Parsers() {
    return this.parsers;
  }

  /**
   * Get Backend name
   * @return {string}
   */
  get Name() {
    return this.name;
  }

  /**
   * Add new job to backend
   * @param options
   * @return {Job}
   */
  addJob(options) {
    const job = new Job({ options });
    this.jobs.set(job.Id, job);
    // eslint-disable-next-line no-restricted-syntax
    for (const ParserConstructor of this.parsers) {
      const parser = new ParserConstructor();
      const workerPromise = this.workersPool.acquire();
      workerPromise
        .then(
          /**
           * Process job
           * @param {Worker} worker
           * @return Promise<void>
           */
          async worker => {
            await worker.runJob(parser, job);
            this.workersPool.release(worker);
          }
        )
        .catch(e => {
          logger.error(e);
        })
        .finally(() => {
          job.setState('finished');
          // delete jobs from list, but with delay
          setTimeout(
            () => this.jobs.delete(job.Id),
            config.Backend.finishedJobDeleteInterval
          );
        });
    }

    return job;
  }
}

module.exports = Backend;
