const Timeout = require('await-timeout');
const { sendInsuranceResult } = require('./ApiHandler');
const { Worker: workerConfig } = require('../config');
const Logger = require('../common/Logger');

const { Timeout: timeoutConfig } = workerConfig;

let id = 0;
class Worker {
  constructor() {
    this.id = id;
    id += 1;
    this.logger = Logger(`src/services/Worker.js=${this.id}`);
  }

  clean() {
    this.logger.debug(`Cleaning...`);
  }

  /**
   * Run job
   * @param {ParserBase} parser
   * @param {Job} job
   * @return {Promise<void>}
   */
  async runJob(parser, job) {
    job.setState('running');
    try {
      const initPromise = parser.Init();
      await Timeout.wrap(
        initPromise,
        timeoutConfig,
        `Init timeout: (job id: ${job.Id}, parser name: ${parser.Name})`
      );
      const runPromise = parser.Run(job);
      const result = await Timeout.wrap(
        runPromise,
        timeoutConfig,
        `Run timeout: (job id: ${job.Id}, parser name: ${parser.Name})`
      );
      this.logger.debug(
        `Sending result ${JSON.stringify(result)} for job ${job.Id} by parser ${
          parser.Name
        } to API`
      );
      const apiResponseObj = await sendInsuranceResult(result);
      job.addParsersResult(parser, {
        parserResult: result,
        apiResponse: apiResponseObj,
      });
    } catch (e) {
      this.logger.error(e);
      job.addParsersResult(parser, null, e.message);
    } finally {
      await parser.Clean();
    }
  }
}

module.exports = Worker;
