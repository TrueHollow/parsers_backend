// eslint-disable-next-line max-classes-per-file
const puppeteer = require('puppeteer-extra');
const pluginStealth = require('puppeteer-extra-plugin-stealth');

const ParserBase = require('./ParserBase');
const config = require('../../config');
const logger = require('../../common/Logger')(
  'src/services/Parsers/CalifAAAInsurance.js'
);

puppeteer.use(pluginStealth());

const INSURANCE_URL =
  'https://apps.calif.aaa.com/aceapps/insurance/quotes/auto/GetStarted';
const TYPE_DELAY = 50;
const LONG_DELAY_API = 1250;

async function typeValue(page, selector, value, pressEnter = true) {
  await page.click(selector, { delay: TYPE_DELAY });
  await page.keyboard.type(value, { delay: TYPE_DELAY });
  if (pressEnter) {
    await page.keyboard.press('Enter', { delay: TYPE_DELAY });
  }
}

async function enterZipCode(page, data) {
  logger.debug('Typing zip code in modal window (if needed)');
  const selector = '#zipForm input.text-center';
  const needToType = await page.evaluate(s => {
    // eslint-disable-next-line no-undef
    const inputTag = document.querySelector(s);
    if (!inputTag) {
      return false;
    }
    return inputTag.offsetParent !== null;
  }, selector);
  if (needToType) {
    logger.debug('Typing zip code.');
    const { txtAddressZip } = data;
    await typeValue(page, selector, txtAddressZip);
    await page.waitFor(LONG_DELAY_API);
  }
}

async function getValueFromSelectTag(page, selector, str) {
  return page.evaluate(
    (sel, s) => {
      const sLowered = s.toLowerCase();
      let result = '';
      // eslint-disable-next-line no-undef
      const selectTag = document.querySelector(sel);
      const { options } = selectTag;
      // eslint-disable-next-line no-restricted-syntax
      for (const option of options) {
        const text = option.text.toLowerCase();
        if (text === sLowered) {
          result = option.value;
          break;
        }
      }
      return result;
    },
    selector,
    str
  );
}

async function contactInformation(page, data) {
  logger.debug('Setting data to contact information page.');
  await typeValue(page, '#txtFirstName', data.txtFirstName);
  await typeValue(page, '#txtLastName', data.txtLastName);
  await typeValue(page, '#txtAddressStreet', data.txtAddressStreet);
  await typeValue(page, '#txtAddressZip', data.txtAddressZip);
  await page.waitFor(LONG_DELAY_API);
  await page.select('#ddlAddressCity', data.ddlAddressCity);
  const phoneValue = await getValueFromSelectTag(
    page,
    '#ddlPhoneType',
    data.ddlPhoneType ? data.ddlPhoneType : 'Home'
  );
  await page.select('#ddlPhoneType', phoneValue);
  await typeValue(page, '#txtPhone', data.txtPhone);
  await typeValue(page, '#txtEmailAddress', data.txtEmailAddress);
  if (Number(data.homeownersDiscount) === 1) {
    await page.click('#chkHomeOwnersInsurance', { delay: TYPE_DELAY });
  }
  await Promise.all([
    page.waitForNavigation({ waitUntil: 'networkidle2' }),
    page.click('#formContainer button.nextBtn', { delay: TYPE_DELAY }),
  ]);
  await page.waitFor(LONG_DELAY_API);
}

async function vehicleInformation(page, data) {
  logger.debug('Setting data to vehicle information page.');
  await page.select('#ddlVehicleYear', data.ddlVehicleYear);
  await page.waitFor(LONG_DELAY_API);
  const makeValue = await getValueFromSelectTag(
    page,
    '#ddlVehicleMake',
    data.ddlVehicleMake
  );
  await page.select('#ddlVehicleMake', makeValue);
  await page.waitFor(LONG_DELAY_API);
  const modelValue = await getValueFromSelectTag(
    page,
    '#ddlVehicleModel',
    data.ddlVehicleModel
  );
  await page.select('#ddlVehicleModel', modelValue);
  await page.waitFor(LONG_DELAY_API);
  await page.click('#rdoHasSalvageTitleNo', { delay: TYPE_DELAY });
  const primaryUseValue = await getValueFromSelectTag(
    page,
    '#ddlPrimaryUse',
    'Commute'
  );
  await page.select('#ddlPrimaryUse', primaryUseValue);
  const ownedTimeValue = await getValueFromSelectTag(
    page,
    '#ddlOwnedTime',
    'Over 3 months'
  );
  await page.select('#ddlOwnedTime', ownedTimeValue);
  await typeValue(page, '#txtAnnualMiles', data.txtAnnualMiles, false);

  // Will change in future.
  await page.click('#rdoAdditionalNo', { delay: TYPE_DELAY });

  await Promise.all([
    page.waitForNavigation({ waitUntil: 'networkidle2' }),
    page.click('#btnAddNextLabel', { delay: TYPE_DELAY }),
  ]);
  await page.waitFor(LONG_DELAY_API);
}

async function addDriver(page, data) {
  logger.debug('Setting data to add driver page.');
  const gender = data.gender.toLowerCase();
  switch (gender) {
    case 'male':
      await page.click('#gender_1', { delay: TYPE_DELAY });
      break;
    case 'female':
      await page.click('#gender_0', { delay: TYPE_DELAY });
      break;
    default:
      await page.click('#gender_2', { delay: TYPE_DELAY });
  }
  const maritalValue = await getValueFromSelectTag(
    page,
    '#ddlMaritalStatus',
    data.ddlMaritalStatus ? data.ddlMaritalStatus : 'Single'
  );
  await page.select('#ddlMaritalStatus', maritalValue);
  await typeValue(page, '#txtDOB', data.txtDOB);

  await typeValue(page, '#txtAgeFirstLicensed', '16'); // always ?
  await page.click('#rdoHasBooDriverRecordNo', { delay: TYPE_DELAY });
  await page.click('#rdoAdditionalNo', { delay: TYPE_DELAY });

  const needToClick = await page.evaluate(s => {
    // eslint-disable-next-line no-undef
    const inputTag = document.querySelector(s);
    if (!inputTag) {
      return false;
    }
    return inputTag.offsetParent !== null;
  }, '#rdoHadDriverTrainingNo');
  if (needToClick) {
    page.click('#rdoHadDriverTrainingNo', { delay: TYPE_DELAY });
  }

  await Promise.all([
    page.waitForNavigation({ waitUntil: 'networkidle2' }),
    page.click('#btnAddNextLabel', { delay: TYPE_DELAY }),
  ]);
  await page.waitFor(LONG_DELAY_API);
}

async function coverages(page, data) {
  logger.debug('Setting data to coverages page.');
  const coverageLimit = data.coverageLimit.toLowerCase();
  switch (coverageLimit) {
    case 'low':
      await page.click('#sltDefCovLow', { delay: TYPE_DELAY });
      break;
    case 'medium':
      await page.click('#sltDefCovMedium', { delay: TYPE_DELAY });
      break;
    default:
      await page.click('#sltDefCovHigh', { delay: TYPE_DELAY });
  }

  await page.waitFor(LONG_DELAY_API);
  await Promise.all([
    page.waitForNavigation({ waitUntil: 'networkidle2' }),
    page.click('#RequestQuoteBtnDsktp', { delay: TYPE_DELAY }),
  ]);
  await page.waitFor(LONG_DELAY_API);
}

async function getPrice(page) {
  logger.debug('Parsing result page.');
  return page.evaluate(() => {
    // eslint-disable-next-line no-undef
    const priceElement = document.querySelector('#quoteUser .amount');
    if (!priceElement) {
      return null;
    }
    const textValue = priceElement.textContent;
    const matches = textValue.match(/([\d.]+)/);
    const floatValue = Number(matches[1]);
    return {
      textValue,
      floatValue,
    };
  });
}

/**
 * Get value from object or throw error
 * @param {Object} obj
 * @param {string} key
 * @throws {Error}
 * @return {*}
 */
const getValueOrThrowError = (obj, key) => {
  if (typeof obj[key] === 'undefined') {
    throw new Error(
      `This option object do not contain necessary field ${key}. Parser cannot run.`
    );
  }
  return obj[key];
};

class ParserValidDataClass {
  /**
   * Create valid options data object or throw error if data now valid
   * @param {Object} rawData
   */
  constructor(rawData) {
    this.contactInformation = {
      txtFirstName: getValueOrThrowError(rawData, 'txtFirstName'),
      txtLastName: getValueOrThrowError(rawData, 'txtLastName'),
      txtAddressStreet: getValueOrThrowError(rawData, 'txtAddressStreet'),
      txtAddressZip: getValueOrThrowError(rawData, 'txtAddressZip'),
      ddlAddressCity: getValueOrThrowError(rawData, 'ddlAddressCity'),
      ddlPhoneType: getValueOrThrowError(rawData, 'ddlPhoneType'), // optional
      txtPhone: getValueOrThrowError(rawData, 'txtPhone'),
      txtEmailAddress: getValueOrThrowError(rawData, 'txtEmailAddress'),
      homeownersDiscount: 1, // if set to 1 (Number)
    };
    this.vehicleInformation = {
      ddlVehicleYear: getValueOrThrowError(rawData, 'ddlVehicleYear'),
      ddlVehicleMake: getValueOrThrowError(rawData, 'ddlVehicleMake'),
      ddlVehicleModel: getValueOrThrowError(rawData, 'ddlVehicleModel'),
      txtAnnualMiles: getValueOrThrowError(rawData, 'txtAnnualMiles'),
    };
    this.driver = {
      gender: getValueOrThrowError(rawData, 'gender'),
      ddlMaritalStatus: getValueOrThrowError(rawData, 'ddlMaritalStatus'),
      txtDOB: getValueOrThrowError(rawData, 'txtDOB'),
    };
    this.coverages = {
      coverageLimit: getValueOrThrowError(rawData, 'coverageLimit'), // medium or high
    };
  }
}

class CalifAAAInsurance extends ParserBase {
  constructor() {
    super();
    this.name = 'CalifAAAInsurance script';
    /**
     * @type {null|Puppeteer.Browser}
     */
    this.browser = null;
    this.page = null;
  }

  async Init() {
    await super.Init();
    logger.debug('Creating new browser');
    this.browser = await puppeteer.launch(config.chrome);
  }

  async Run(job) {
    await super.Run(job);
    const { options: rawData } = job;
    const data = new ParserValidDataClass(rawData);
    logger.debug('Invoking getPrice');
    const page = await this.browser.newPage();
    this.page = page;
    await page.setViewport({ width: 1920, height: 1040 });
    logger.debug('Opening insurance site.');
    await page.goto(INSURANCE_URL, { waitUntil: 'networkidle2' });
    await enterZipCode(page, data.contactInformation);
    await contactInformation(page, data.contactInformation);
    await vehicleInformation(page, data.vehicleInformation);
    await addDriver(page, data.driver);
    await coverages(page, data.coverages);
    const price = await getPrice(page);
    await page.close();
    this.page = null;
    const { floatValue } = price;
    return {
      price: floatValue,
      otherthing: 'uniqueidwillbelongstring1234234', // todo: this is temp. Should be removed as soon as possible
    };
  }

  async Clean() {
    await super.Clean();
    const { browser, page } = this;
    if (page) {
      try {
        this.page = null;
        await page.close();
        logger.debug(
          `Frozen page is closed is closed ${
            this.Name
          } (${browser.isConnected()})`
        );
      } catch (e) {
        logger.error(e);
      }
    }
    if (browser) {
      try {
        this.browser = null;
        await browser.close();
        logger.debug(
          `Browser is closed ${this.Name} (${browser.isConnected()})`
        );
      } catch (e) {
        logger.error(e);
      }
    }
  }
}

module.exports = CalifAAAInsurance;
