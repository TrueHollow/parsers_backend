const ParserBase = require('./ParserBase');
const logger = require('../../common/Logger')(
  'src/services/Parsers/ExampleParser.js'
);

const ExampleResult = {
  price: 253.23,
  otherthing: 'uniqueidwillbelongstring1234234',
};

class ExampleParser extends ParserBase {
  constructor() {
    super();
    this.name = 'ExampleParser';
  }

  async Run(job) {
    await super.Run(job);
    if (Object.keys(job.options).length) {
      logger.warn(
        `This parser should not be used for actual parameters and should be disabled in production mode`
      );
    }
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(ExampleResult);
      }, 10000);
    });
  }
}

module.exports = ExampleParser;
