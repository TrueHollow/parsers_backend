const logger = require('../../common/Logger')(
  'src/services/Parsers/ParserBase.js'
);

class ParserBase {
  constructor() {
    this.name = 'ParserBase';
  }

  /**
   * Get parser name
   * @return {string}
   */
  get Name() {
    return this.name;
  }

  /**
   * Initialization stage (prepare browser for example).
   * This stage CAN throw error
   * @return {Promise<void>}
   */
  async Init() {
    logger.debug(`Init ${this.name} parser`);
  }

  /**
   * Cleaning stage (close browser instance for example)
   * This stage SHOULD NOT throw error.
   * @return {Promise<void>}
   */
  async Clean() {
    logger.debug(`Clean ${this.name} parser`);
  }

  /**
   * Run job on parser
   * @param {Job} job
   * @return {Promise<Object>}
   */
  async Run(job) {
    logger.debug(`Running job ${job.id} on parser ${this.name}`);
  }
}

module.exports = ParserBase;
