const { v4: uuidv4 } = require('uuid');
const logger = require('../common/Logger')('src/services/Job.js');

class Job {
  /**
   * Get summary
   * @param {Object} params
   * @param {options} options - Script options
   * @constructor * */
  constructor({ options }) {
    /**
     * @type {string}
     */
    this.id = uuidv4();
    this.options = options;
    this.state = 'pending';
    this.timeCreation = new Date();
    this.timeStart = null;
    this.timeEnd = null;
    this.parsersResult = [];
    logger.debug(`Job ${this.id} is created.`);
  }

  /**
   * Set state value string
   * @param {string} value
   */
  setState(value) {
    if (this.state === value) {
      return; // ignore resetting with same value
    }
    logger.debug(`Job ${this.id} changed state to ${value}`);
    switch (value.toLowerCase()) {
      case 'running':
        this.timeStart = new Date();
        break;
      case 'finished':
        this.timeEnd = new Date();
        break;
      default:
        logger.warn(
          `Job ${this.id} changed state to unexpected value ${value}`
        );
    }
    this.state = value;
  }

  /**
   * Add parser result to this job
   * @param {ParserBase} parser
   * @param {Object|null} result
   * @param {string|object|null} error
   */
  addParsersResult(parser, result, error = null) {
    this.parsersResult.push({
      result,
      parserName: parser.Name,
      error,
    });
  }

  /**
   * Get Job Id
   * @return {string}
   */
  get Id() {
    return this.id;
  }
}

module.exports = Job;
