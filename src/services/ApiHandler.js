const needle = require('needle');

/**
 * Send result to api endpoint
 * @param {Object} result
 * @return {Promise<Object>}
 */
const sendInsuranceResult = async result => {
  const response = await needle(
    'post',
    'https://app.quotemingle.com/version-test/api/1.1/obj/apitest/',
    result,
    { multipart: true }
  );
  const { body } = response;
  return body;
};

module.exports = {
  sendInsuranceResult,
};
