const env = require('env-var');

const WORKERS_POOL_MIN_SIZE = env
  .get('WORKERS_POOL_MIN_SIZE')
  .default(1)
  .asIntPositive();

const WORKERS_POOL_MAX_SIZE = env
  .get('WORKERS_POOL_MAX_SIZE')
  .default(10)
  .asIntPositive();

const JOB_DELETE_INTERVAL = env
  .get('JOB_DELETE_INTERVAL')
  .default(10000)
  .asIntPositive();

const BROWSER_HEADLESS = env.get('BROWSER_HEADLESS').default('true').asBool();

module.exports = {
  /**
   * Logger common configuration
   */
  log4js: {
    appenders: {
      out: {
        type: 'stdout',
      },
    },
    categories: {
      default: {
        appenders: ['out'],
        level: 'debug',
      },
    },
  },
  /**
   * Default express http port
   */
  express: {
    http_port: 3000,
  },
  Worker: {
    Timeout: 3 * 60 * 1000, // 3 minutes
  },
  chrome: {
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-infobars',
      '--window-position=0,0',
      '--ignore-certifcate-errors',
      '--ignore-certifcate-errors-spki-list',
    ],
    headless: BROWSER_HEADLESS,
    ignoreHTTPSErrors: true,
  },
  Backend: {
    finishedJobDeleteInterval: JOB_DELETE_INTERVAL,
    Pool: {
      options: {
        min: WORKERS_POOL_MIN_SIZE, // minimum size of the pool
        max: WORKERS_POOL_MAX_SIZE, // maximum size of the pool
      },
    },
  },
};
