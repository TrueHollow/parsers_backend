require('./common/dotEnv_proxy');
const path = require('path');
const express = require('express');
const httpCodes = require('http-status-codes');
const config = require('./config');
const middleware = require('./middleware');
const Backend = require('./services/Backend');
const routes = require('./routes');
const logger = require('./common/Logger')('src/index.js');

/**
 * Express application
 * @type {app}
 */
const app = express();

app.set('trust proxy', 1); // trust first proxy (used for Heroku)

const backend = new Backend();
middleware(app, logger);
routes(app, backend);

const staticFilesDirectory = path.resolve(__dirname, 'static');
app.use(express.static(staticFilesDirectory));

app.use('*', (req, res) => {
  res.status(httpCodes.NOT_FOUND);
  res.json({
    message: 'Not found',
  });
});

app.use((error, req, res /* next */) => {
  res.status(error.status || httpCodes.INTERNAL_SERVER_ERROR);
  logger.error(error);
  res.json({
    status: error.status,
    message: error.message,
    // stack: error.stack,
  });
});

const httpPort = process.env.PORT || config.express.http_port;

const listener = app.listen(httpPort);
logger.info(`app running on port http://localhost:${httpPort} ...`);

module.exports = { app, listener, backend };
