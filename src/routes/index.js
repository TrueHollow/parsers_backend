const env = require('env-var');
const config = require('../config');

const SHOW_CONFIG_ENDPOINT = env
  .get('SHOW_CONFIG_ENDPOINT')
  .default('false')
  .asBool();

/**
 *
 * @param {Function} app
 * @param {Backend} backend
 */
module.exports = (app, backend) => {
  app.get('/', (req, res) => {
    return res.json({ message: backend.Name });
  });
  app.get('/jobs', (req, res) => {
    return res.json(backend.Jobs);
  });
  app.get('/parsers', (req, res) => {
    return res.json(backend.Parsers);
  });
  app.post('/api/v1/insurance/prices', (req, res) => {
    const { body: data } = req;
    return res.json(backend.addJob(data));
  });
  if (SHOW_CONFIG_ENDPOINT) {
    app.get('/config', (req, res) => {
      return res.json(config);
    });
  }
};
