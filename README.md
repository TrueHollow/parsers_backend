# Express Parsers Backend

## Environment variables
```dotenv
WORKERS_POOL_MIN_SIZE = 1
WORKERS_POOL_MAX_SIZE = 10
BROWSER_HEADLESS = true
SHOW_CONFIG_ENDPOINT = false # add new edpoint (`/config`) for checking current configuration
JOB_DELETE_INTERVAL = 10000 # interval, after that finished job will be deleted from statistic
```
